# Connexion base
drv <- dbDriver("PostgreSQL")

# Tout les éléments de connexion doivent être personnaliser selon votre configuration. Si vous souhaitez utiliser des couches shape 
# en remplacement des bases postgres, il vous faudra réécrire la partie ouverture de table du script

#connexion à la base postgres contenant le OSO Theia
con_friches <- dbConnect(drv, dbname = "nom de la base",
                         host = "ip de la base", port = 5432,
                         user = "compte", password = "mot de passe")

#connexion à la base postgres fichiers fonciers
con_majic <- dbConnect(drv, dbname = "nom de la base",
                       host = "ip de la base", port = 5432,
                       user = "compte", password = "mot de passe")

#connexion à la base postgres contenant le RPG
con_rpg <- dbConnect(drv, dbname = "nom de la base",
                     host = "ip de la base", port = 5432,
                     user = "compte", password = "mot de passe")

#conversion de l'encodage entre R et postgres
postgresqlpqExec(con_friches, "SET client_encoding = 'Win-1252'")
postgresqlpqExec(con_majic, "SET client_encoding = 'Win-1252'")
postgresqlpqExec(con_rpg, "SET client_encoding = 'Win-1252'")